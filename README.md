# ruby rpm build image

## Description
        
CentOS 7 docker image with ruby build dependencies: 
- rpm-build
- tar
- make
- readline-devel
- ncurses-devel
- gdbm-devel
- glibc-devel
- gcc
- openssl-devel
- libyaml-devel
- libffi-devel
- zlib-devel
      
## purpose
        
builds a docker image with all dependencies to build and package the latest version of ruby with rpmbuild.

---

*Build with [odagrun](https://www.odagun.com) on openshift-online-starter*